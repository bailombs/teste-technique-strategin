# Authentification

This application is fullstack web application responsive for the users authentication system, Create, login , get all users via token authentication.
This application was developed by nodejs express mongoDb Atlas, mongoose, Reactjs, redux-toolkit and sass

## Prerequisites

- [Node.js (v16.0.0) +](https://nodejs.org/en/)
- [Recommended IDE (Visual Studio Code)](https://code.visualstudio.com)

## Installation

First of all, clone the project with HTTPS or Clone the repo onto your computer

```bash
  git clone https://gitlab.com/bailombs/authentification.git
```

The second time you need to install dependencies for the Back_end and the Front_end

### Back-end

Need to be inside the root of the project folder, and run these commands (install dependencies, and run locally).

> Always start by run the auth-deploy

```
strateg_in_test Folder
yarn install or npm install
    and
yarn dev:server or npm run dev:server

Your server should now be running at http://locahost:5000  with mongodb connexion reussie!
```

### Front-end

make sure to be in the front_end folder

```
yarn install or npm install
    and
yarn start or npm start
```

## API Documentation

To learn more about how the API works, once you have started your local environment, you can visit: http://localhost:5000/documentation

## Dependencies

| Name               |
| ------------------ |
| react              |
| react-dom          |
| react-scripts      |
| react-icons        |
| react-redux        |
| redux toolkit      |
| axios              |
| jsonwebtoken       |
| Express            |
| mongoose           |
| swagger-ui-express |
| swagger-jsdoc      |
| sass               |

### Test Login

- Email: `bailombs@chezmoi.com`
- Password: `password123`
