import { useLocation } from "react-router-dom";

export const useCurrentPage = () => {
    const location = useLocation()

    let page = '';

    if (location?.pathname === '/') {
        page = 'Connexion';
    } else if (location?.pathname === '/login') {
        page = 'Creer un compte'
    } else {
        page = 'Deconnexion'
    }
    return page;
} 