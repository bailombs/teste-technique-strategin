import React from 'react';
import { useSelector } from 'react-redux';
import { Navigate, Outlet } from 'react-router-dom';

const PrivateRoute = () => {

    const token = useSelector(state => state?.connect?.token)

    const useAuth = () => {

        const user = { loggedIn: token }
        return user && user.loggedIn
    }

    const isAuth = useAuth()
    return (

        isAuth ? <Outlet /> : <Navigate to='/login' />
    );
};

export default PrivateRoute;