import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { getRegisterSucess } from '../redux/slice';
import baseUrl from "../services/routes";


const RegisterForm = () => {
    const [errorMessage, setErrorMessage] = useState('')
    const [registerName, setRegisterName] = useState('')
    const [registerLastName, setRegisterLastName] = useState('')
    const [registerEmail, setRegisterEmail] = useState('')
    const [registerPassword, setRegisterPassword] = useState('')
    const [isSuccess, setIsSuccess] = useState(false)

    const dispatch = useDispatch()
    const registerSuccess = useSelector((state) => state?.connect?.registerSuccess)

    useEffect(() => {
        setErrorMessage('')
    }, [registerName, registerLastName, registerEmail, registerPassword])

    useEffect(() => {
        if (isSuccess) {
            setTimeout(
                () => {
                    setIsSuccess(false)
                }, 4000)
        }
    }, [isSuccess])

    let formatMailRegex = /^[a-z0-9.-_]+[@]{1}[a-z.-_]+[.]{1}[a-z]{2,8}$/

    // function register to the dataBase
    const handleRegister = async (e) => {
        e.preventDefault()
        if (registerName === "") {
            setErrorMessage("Votre prénom est requis");
            return
        } else if (registerLastName === "") {
            setErrorMessage("Votre nom est requis");
            return
        } else if (formatMailRegex.test(registerEmail) === false) {
            setErrorMessage("Veuillez une adresse mail valide");
            return
        } else if (registerPassword === "") {
            setErrorMessage("Votre entrer un mot de passe");
        }
        try {
            const res = await baseUrl.post('/register', {

                "email": registerEmail,
                "password": registerPassword,
                "firstName": registerName,
                "lastName": registerLastName
            })

            dispatch(getRegisterSucess(res.data.message));
            setIsSuccess(true)
            setRegisterName(' ')
            setRegisterLastName(' ')
            setRegisterEmail(' ')
            setRegisterPassword(' ')
        } catch (error) {
            if (error?.response.status === 400) {
                setErrorMessage('Pas de reponse de serveur')
            }
            else if (error?.response?.status === 500) {
                setErrorMessage(' Erreur serveur')
            } else if (!error?.response) {
                setErrorMessage('Pas de reponse du server')
            } else if (error?.response?.status === 401) {
                setErrorMessage('Votre mail ou mot de passe ne correpondent pas')
            }
        }
    }

    return (

        <section className='register'>
            <h1 className='register__title'>Creer un compte</h1>
            <form className='register__form' onSubmit={(e) => { handleRegister(e) }} >
                <div className='register__form--inptgroup'>
                    <input
                        type="text" id='firstName'
                        onChange={(e) => { setRegisterName(e.target.value) }}
                        value={registerName}
                        autoComplete="off"
                        placeholder=' '
                    />
                    <label htmlFor='firstName'>Prénom *</label>
                </div>
                <div className='register__form--inptgroup'>
                    <input
                        type="text" id='lastName'
                        onChange={(e) => { setRegisterLastName(e.target.value) }}
                        value={registerLastName}
                        autoComplete="off"
                        placeholder=' '
                    />
                    <label htmlFor='lastName'>Nom *</label>
                </div>
                <div className='register__form--inptgroup'>
                    <input
                        type="text" id='email'
                        onChange={(e) => { setRegisterEmail(e.target.value) }}
                        value={registerEmail}
                        autoComplete="off"
                        placeholder=' '
                    />
                    <label htmlFor='email'>Email *</label>
                </div>
                <div className='register__form--inptgroup'>
                    <input
                        type="password" id='password'
                        onChange={(e) => { setRegisterPassword(e.target.value) }}
                        value={registerPassword}
                        autoComplete="off"
                        placeholder=' '
                    />
                    <label htmlFor='password'>Mot de passe *</label>
                </div>
                {
                    errorMessage ?
                        <p className='register__form--error'>{errorMessage}</p> : null
                }
                <input type="submit" value="valider" className='register__form--valid' />
                <p className='register__form--link'> Vous avez déjà un compte ?
                    &nbsp;<NavLink to="/login">Cliquez-ici</NavLink>
                </p>
            </form>
            {
                isSuccess && registerSuccess &&
                <p className='register__success'>Compte créée avec succes, vous pouvez vous connecter</p>
            }

        </section>

    );
};

export default RegisterForm;