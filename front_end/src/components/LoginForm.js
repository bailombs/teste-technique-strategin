import React, { useEffect, useState } from 'react';
import { FaUser } from 'react-icons/fa';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { getToken, getUserId } from '../redux/slice';
import baseUrl from '../services/routes';


const LoginForm = () => {

    const [errorMessage, setErrorMessage] = useState('')
    const [loginEmail, setLoginEmail] = useState('')
    const [loginPassword, setLoginPassword] = useState('')
    const navigate = useNavigate()
    const dispatch = useDispatch()

    useEffect(() => {
        setErrorMessage('')
    }, [loginEmail, loginPassword])

    let formatMailRegex = /^[a-z0-9.-_]+[@]{1}[a-z.-_]+[.]{1}[a-z]{2,8}$/


    const handleSubmit = async (e) => {
        e.preventDefault()

        if (formatMailRegex.test(loginEmail) === false) {
            setErrorMessage("Veuillez entrer une adresse mail valide");
            return
        } else if (loginPassword === "") {
            setErrorMessage("champ mot de passe requis");
            return
        }

        try {
            const res = await baseUrl.post('/login', {
                "email": loginEmail,
                "password": loginPassword,
            })

            dispatch(getToken(res?.data?.token))
            dispatch(getUserId(res?.data?.userId))
            console.log(res.data);

            // Set token the sessionStorage
            sessionStorage.setItem('user', JSON.stringify(res?.data))

            navigate("/users")
            setLoginEmail('')
            setLoginEmail('')

        } catch (error) {
            if (error?.response.status === 400) {
                setErrorMessage('Pas de reponse de serveur')
            }
            else if (error?.response?.status === 500) {
                setErrorMessage(' Erreur serveur')
            } else if (!error?.response) {
                setErrorMessage('Pas de reponse du server')
            } else if (error?.response?.status === 401) {
                setErrorMessage('Votre mail ou mot de passe ne correpondent pas')
            }
        }
    }

    return (
        <section className='register'>
            <FaUser />
            <h1 className='register__title'>Connexion</h1>
            <form className='register__form' onSubmit={(e) => { handleSubmit(e) }} >

                <div className='register__form--inptgroup'>
                    <input
                        type="text" id='email'
                        onChange={(e) => { setLoginEmail(e.target.value) }}
                        placeholder=' '
                        autoComplete="off"
                    />
                    <label htmlFor='email'>Email</label>
                </div>
                <div className='register__form--inptgroup'>
                    <input
                        type="password" id='password'
                        onChange={(e) => { setLoginPassword(e.target.value) }}
                        placeholder=' '
                        autoComplete="off"
                    />
                    <label htmlFor='password'>Mot de passe</label>
                </div>
                {
                    errorMessage ?
                        <p className='register__form--error'>{errorMessage}</p> : null
                }

                <input type="submit" value="Se connecter" className='register__form--valid' />
            </form>
        </section>
    );
};

export default LoginForm;