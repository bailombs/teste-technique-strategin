import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Navigate, useLocation } from 'react-router-dom';
import { getFirstName } from '../redux/slice';
import baseUrl from '../services/routes';
import Card from './Card';

const ListUsers = () => {
    const [users, setUsers] = useState([])
    const dispatch = useDispatch()
    const location = useLocation()


    const id = useSelector((state) => state?.connect?.userId)
    const currentUserId = users?.find((user) => user._id === id)
    /**
     * get the tok from the sessionStorage
     */
    // const user = JSON.parse(sessionStorage.getItem('user'))
    const token = useSelector((state) => state?.connect?.token)

    useEffect(() => {
        if (token)
            dispatch(getFirstName(currentUserId?.firstName))
    }, [currentUserId, dispatch, token])

    // get all users from the Api with the token
    useEffect(() => {
        baseUrl.get('/users', {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        }).then((res) => setUsers(res.data))


    }, [token])

    useEffect(() => {
        if (token) {
            <Navigate to='/users' state={{ from: location }} replace />
        }
    }, [token, location])


    return (
        <section className='users'>
            <h1 className='users__title'>Liste des utilisateurs</h1>
            {
                users?.map((user) => (
                    <Card key={user?._id} user={user} />
                )).reverse()
            }
        </section>
    );
};

export default ListUsers;