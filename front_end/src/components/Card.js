import React from 'react';

const Card = ({ user }) => {
    return (
        <ul className='users__card'>
            <li className='users__card--item'><b>Id:</b> {`${user?._id}`}</li>
            <li className='users__card--item'><b>Email:</b> {` ${user?.email}`}</li>
            <li className='users__card--item'> <b>Nom:</b>{` ${user?.lastName}`}</li>
            <li className='users__card--item'> <b>Prénom:</b>{` ${user?.firstName}`}</li>
        </ul>
    );
};

export default Card;