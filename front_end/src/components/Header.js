import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { logOut } from "../redux/slice";
import { useCurrentPage } from "../utils/utils";

const Header = () => {

    let currentLabel = useCurrentPage()
    const dispatch = useDispatch()
    const firstName = useSelector((state) => state?.connect?.firstName)

    return (
        <header className='header'>
            <h1 className='header__title'>Système d'Authentification</h1>

            {currentLabel === "Deconnexion" && <p className="header__profile">{firstName}</p>}

            <div className='header__link'>
                <NavLink to={currentLabel === "Connexion" ? "/login" : "/"}
                    className='header__link--link'
                    onClick={() => {
                        dispatch(logOut())
                        sessionStorage.removeItem("user")
                    }}
                >
                    {currentLabel}
                </NavLink>
            </div>
        </header>
    );
};

export default Header;