import { configureStore } from "@reduxjs/toolkit";
import connectReducer from './slice';

export default configureStore({
    reducer: {
        connect: connectReducer,
    }
})