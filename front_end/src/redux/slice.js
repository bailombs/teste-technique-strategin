import { createSlice } from '@reduxjs/toolkit';

const user = JSON.parse(sessionStorage.getItem('user'))
const token = user?.token
const id = user?.userId

export const slice = createSlice({
    name: 'authentification',
    initialState: {
        userId: id,
        token: token,
        firstName: '',
        registerSuccess: "",
    },
    reducers: {
        getUserId: (state, action) => {
            state.userId = action.payload
        },
        getFirstName: (state, action) => {
            state.firstName = action.payload;
        },
        getToken: (state, action) => {
            state.token = action.payload
        },
        getRegisterSucess: (state, action) => {
            state.registerSuccess = action.payload;
        },
        logOut: (state) => {
            state.token = null;
            state.firstName = "";
            state.userId = null
        }
    }
})

export default slice.reducer;
export const { getUserId, getFirstName, getToken, getRegisterSucess, logOut } = slice.actions 