import React from 'react';
import Header from '../components/Header';
import RegisterForm from '../components/RegisterForm';

const Register = () => {
    return (
        <>
            <Header />
            <RegisterForm />
        </>
    );
};

export default Register;