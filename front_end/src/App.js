import React from 'react';
import { Route, Routes } from 'react-router-dom';
import PrivateRoute from './components/PrivateRoute';
import Login from './pages/Login';
import Register from './pages/Register';
import Users from './pages/Users';


const App = () => {
  return (
    <Routes>
      <Route path='/' element={<Register />} />
      <Route path='/login' element={<Login />} />

      {/* private route  */}
      <Route element={<PrivateRoute />} >
        <Route path='/users' element={<Users />} />
      </Route>
    </Routes>
  );
};

export default App;