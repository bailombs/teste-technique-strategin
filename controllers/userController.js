const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const userModel = require('../models/userModel')
require('dotenv').config()

module.exports.createUser = (req, res) => {
    bcrypt.hash(req.body.password, 10)
        .then((hash) => {
            const user = new userModel({
                email: req.body.email,
                password: hash,
                firstName: req.body.firstName,
                lastName: req.body.lastName
            })
            user.save()
                .then(() => res.status(201).json({ message: 'utilisateur créée' }))
                .catch((error) => res.status(400).json({ error }))
        })
        .catch((error) => res.status(400).json({ error }))
}

module.exports.login = (req, res) => {
    userModel.findOne({ email: req.body.email })
        .then((user) => {
            if (user === null) {
                res.status(401).json({ message: 'identifiant ou mot de passe incorrecte' })
            } else {
                bcrypt.compare(req.body.password, user.password)
                    .then((valid) => {
                        if (!valid) {
                            res.status(401).json({ message: 'identifiant ou mot de passe incorrecte' })
                        } else {
                            res.status(200).json({
                                userId: user._id,
                                token: jwt.sign(
                                    { userId: user._id },
                                    process.env.RANDOM_TOKEN_SECRET,
                                    { expiresIn: '24h' }
                                )
                            })
                        }
                    })
                    .catch((error) => res.status(500).json({ error }))
            }
        })
        .catch((error) => res.status(500).json({ error }))
}

module.exports.getUsers = (req, res) => {
    userModel.find()
        .then((users) => res.status(200).json(users))
        .catch((error) => res.status(400).json({ error }))
}