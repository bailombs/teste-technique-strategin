const express = require('express')
const mongoose = require('mongoose')
const userRoute = require('./routes/userRoute')
const app = express()
const cors = require('cors')
const swaggerDoc = require('swagger-ui-express')
const swaggerDocumentation = require('./swagger/swagger.json')


app.use(express.json())


mongoose.connect('mongodb+srv://bailombs:bambeto2010@cluster0.ijzdwrl.mongodb.net/?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
    .then(() => console.log("connexion à mongodb réussie"))
    .catch(() => console.log('connexion à mongodb echoué'))

app.use(cors())
app.use('/documentation/', swaggerDoc.serve)
app.use('/documentation/', swaggerDoc.setup(swaggerDocumentation))
app.use('/api/auth', userRoute);

module.exports = app;