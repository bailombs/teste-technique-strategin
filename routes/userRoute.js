const express = require('express')
const router = express.Router()
const userController = require('../controllers/userController')
const auth = require('../middleware/tokenValidation')


router.post('/register', userController.createUser)
router.post('/login', userController.login)
router.get('/users', auth, userController.getUsers)


module.exports = router;