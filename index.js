const http = require('http')
const app = require('./app')
const express = require('express')
const path = require('path')

const PORT = process.env.PORT || 5000;


app.set('port', process.env.PORT || 5000)

const server = http.createServer(app)

app.use(express.static(path.join(__dirname, 'front_end/build')))
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'front_end', 'build', 'index.html'))
})

server.listen(PORT, () => {
    console.log(`Server started on http://localhost:${PORT}`)
})